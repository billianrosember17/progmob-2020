package ukdw.com.progmob2020.UTS_CRUD_Matkul;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Model.Matkul;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;
import ukdw.com.progmob2020.UTS_Adapter.MatkulCRUDRecyclerAdapter;

public class LihatMatkulActivity extends AppCompatActivity {
    RecyclerView rvMk;
    MatkulCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_lihat);

        rvMk = (RecyclerView)findViewById(R.id.rvLihatMk);
        pd = new ProgressDialog(this);
        pd.setTitle("Loading");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180240");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapter = new MatkulCRUDRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatMatkulActivity.this);
                rvMk.setLayoutManager(layoutManager);
                rvMk.setAdapter(matkulAdapter);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LihatMatkulActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}