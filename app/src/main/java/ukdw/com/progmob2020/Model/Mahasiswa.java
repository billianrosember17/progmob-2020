package ukdw.com.progmob2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mahasiswa {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("nim")
    @Expose
    private String nim;
    @SerializedName("notelp")
    @Expose
    private String notelp;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("no_progmob")
    @Expose
    private String no_progmob;


    public Mahasiswa(String id, String nama, String nim, String notelp, String alamat, String email, String foto, String no_progmob) {
        this.id = id;
        this.nama = nama;
        this.nim = nim;
        this.notelp = notelp;
        this.alamat = alamat;
        this.email = email;
        this.foto = foto;
        this.no_progmob = no_progmob;
    }

    public Mahasiswa(String nama, String nim, String notelp, String alamat, String email, String foto, String no_progmob) {
        this.nama = nama;
        this.nim = nim;
        this.notelp = notelp;
        this.alamat = alamat;
        this.email = email;
        this.foto = foto;
        this.no_progmob = no_progmob;
    }

    public Mahasiswa(String nama, String nim, String notelp) {
        this.nama = nama;
        this.nim = nim;
        this.notelp = notelp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNo_progmob() {
        return no_progmob;
    }

    public void setNo_progmob(String no_progmob) {
        this.no_progmob = no_progmob;
    }



    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

}
