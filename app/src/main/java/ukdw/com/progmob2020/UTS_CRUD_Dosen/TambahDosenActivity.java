package ukdw.com.progmob2020.UTS_CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Model5.DefaultResult;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class TambahDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_tambah);

        final EditText edNama = (EditText)findViewById(R.id.editTextNamaDsn);
        final EditText edNidm = (EditText)findViewById(R.id.editTextNidn);
        final EditText edAlamat = (EditText)findViewById(R.id.editTextAlamatDsn);
        final EditText edEmail = (EditText)findViewById(R.id.editTextEmailDsn);
        final EditText edGelar = (EditText)findViewById(R.id.editTextGelarDsn);
        Button btnSimpan = (Button)findViewById(R.id.btnSave);
        pd = new ProgressDialog(TambahDosenActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dsn(
                        edNama.getText().toString(),
                        edNidm.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        edGelar.getText().toString(),
                        "Kosongkan",
                        "72180240"

                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this, "Data Berhasil Disimpan", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(TambahDosenActivity.this, "Data Gagal Disimpan", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}