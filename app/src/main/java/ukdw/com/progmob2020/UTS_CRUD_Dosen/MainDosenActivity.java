package ukdw.com.progmob2020.UTS_CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_main);

        Button btnLihat = (Button)findViewById(R.id.btnLihatDsn);
        Button btnTambah = (Button)findViewById(R.id.btnTambahDsn);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdateDsn);
        Button btnDelete = (Button)findViewById(R.id.btnDeleteDsn);

        btnLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, LihatDosenActivity.class);
                startActivity(intent);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, TambahDosenActivity.class);
                startActivity(intent);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainDosenActivity.this, UpdateDosenActivity.class);
                startActivity(intent);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DeleteDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}