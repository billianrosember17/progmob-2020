package ukdw.com.progmob2020.UTS_CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Model5.DefaultResult;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class UpdateDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        EditText edCariNidn = (EditText)findViewById(R.id.editTextCariNidn);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTextNamaBaruDsn);
        EditText edNidnBaru = (EditText)findViewById(R.id.editTextNidnBaru);
        EditText edAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaruDsn);
        EditText edEmailBaru = (EditText)findViewById(R.id.editTextEmailBaruDsn);
        EditText edGelarBaru = (EditText)findViewById(R.id.editTextGelarBaruDsn);
        Button btnUp = (Button)findViewById(R.id.btnUpdateBaruDsn);
        pd = new ProgressDialog(UpdateDosenActivity.this);

        Intent data = getIntent();
        edCariNidn.setText(data.getStringExtra("nidn"));
        edNamaBaru.setText(data.getStringExtra("nama"));
        edNidnBaru.setText(data.getStringExtra("nidn"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));
        edGelarBaru.setText(data.getStringExtra("gelar"));

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dsn(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "Kosongkan",
                        "72180240",
                        edCariNidn.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(UpdateDosenActivity.this, " Berhasil Diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(UpdateDosenActivity.this, " Gagal Diupdate", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateDosenActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}