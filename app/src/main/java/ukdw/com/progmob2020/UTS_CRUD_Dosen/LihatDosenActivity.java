package ukdw.com.progmob2020.UTS_CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.UTS_Adapter.DosenCRUDRecyclerAdapter;
import ukdw.com.progmob2020.UTS_CRUD_Dosen.LihatDosenActivity;
import ukdw.com.progmob2020.Model.Dosen;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class LihatDosenActivity extends AppCompatActivity {
    RecyclerView rvDsn;
    DosenCRUDRecyclerAdapter dsnAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_lihat);

        rvDsn = (RecyclerView)findViewById(R.id.rvLihatDsn);
        pd = new ProgressDialog(this);
        pd.setTitle("Loading");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180240");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dsnAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatDosenActivity.this);
                rvDsn.setLayoutManager(layoutManager);
                rvDsn.setAdapter(dsnAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LihatDosenActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}