package ukdw.com.progmob2020.UTS_Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import ukdw.com.progmob2020.R;

public class UtsMainActivity extends AppCompatActivity {
    private int waktu_loading=4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent home = new Intent(UtsMainActivity.this, UtsPrefActivity.class);
                startActivity(home);
                finish();
            }
        },waktu_loading);
    }
}