package ukdw.com.progmob2020.UTS_Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import ukdw.com.progmob2020.CRUD.MainMahasiswaActivity;
import ukdw.com.progmob2020.R;
import ukdw.com.progmob2020.UTS_CRUD_Dosen.MainDosenActivity;
import ukdw.com.progmob2020.UTS_CRUD_Matkul.MainMatkulActivity;

public class UtsMainLoginActivity extends AppCompatActivity {
    String isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_main_login);

        TextView tampil = (TextView)findViewById(R.id.textViewWelcome);
        ImageButton btnOut = (ImageButton) findViewById(R.id.imageButtonLogout);
        ImageButton btnDsn = (ImageButton) findViewById(R.id.imageButtonDsn);
        ImageButton btnMhs = (ImageButton) findViewById(R.id.imageButtonMhs);
        ImageButton btnMk = (ImageButton) findViewById(R.id.imageButtonMatkul);

        Bundle b = getIntent().getExtras();
        String textHelp = b.getString("help_string");
        tampil.setText(textHelp);

        SharedPreferences pref = UtsMainLoginActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        btnOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UtsMainLoginActivity.this, UtsPrefActivity.class);
                startActivity(intent);
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                }else{
                    editor.putString("isLogin","0");
                }
                editor.commit();
                /*new AlertDialog.Builder(getApplication())
                        .setTitle("Apakah Kamu Ingin Keluar?")
                        .setMessage("Ingin Keluar Dari Aplikasi Ini?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "Anda Telah Logout", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(UtsLoginMainActivity.this, PrefActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplication(), "Anda Tidak Jadi Keluar", Toast.LENGTH_LONG).show();
                            dialog.cancel();
                    }
                }).show();*/
            }
        });

        btnMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UtsMainLoginActivity.this, MainMahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UtsMainLoginActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });

        btnMk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UtsMainLoginActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}