package ukdw.com.progmob2020.UTS_Login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ukdw.com.progmob2020.R;

public class UtsPrefActivity extends AppCompatActivity {
    String isLogin;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_pref);

        Button btnPref3 = (Button)findViewById(R.id.btnLogin);

        SharedPreferences pref = UtsPrefActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")){
            btnPref3.setText("Logout");
        }else{
            btnPref3.setText("Login");
        }

        final EditText un = (EditText)findViewById(R.id.editTeksUser);
        final EditText pass = (EditText)findViewById(R.id.editTeksPass);
        pd = new ProgressDialog(UtsPrefActivity.this);

        btnPref3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                pd.setTitle("Loading");
                pd.show();

                Intent intent = new Intent(UtsPrefActivity.this, UtsMainLoginActivity.class);
                Bundle b = new Bundle();
                b.putString("help_string", un.getText().toString());
                intent.putExtras(b);

                if(un.getText().toString().length() == 0){
                    un.setError("Masukkan Username");
                }else if(pass.getText().toString().length()==0){
                    pass.setError("Masukkan Password");
                }else{
                    Toast.makeText(getApplicationContext(),"Berhasil Masuk", Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }


                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    btnPref3.setText("Logout");
                }else{
                    editor.putString("isLogin","0");
                    btnPref3.setText("Login");
                }

                editor.commit();
            }
        });
    }
}