package ukdw.com.progmob2020.Pertemuan4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob2020.Adapter4.DebuggingRecyclerAdapter;
import ukdw.com.progmob2020.Model.Mahasiswa;
import ukdw.com.progmob2020.R;

public class DebuggingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debugging);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        DebuggingRecyclerAdapter debuggingRecyclerAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Billianro Sember","72180240","08123456789");
        Mahasiswa m2 = new Mahasiswa("Sember Billianro","72180241","08123456789");
        Mahasiswa m3 = new Mahasiswa("Williandro Sember","72180242","08123456789");
        Mahasiswa m4 = new Mahasiswa("Sember Williandro","72180243","08123456789");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);

        //List<Mahasiswa> mahasiswaListDebug = new ArrayList<Mahasiswa>();

        debuggingRecyclerAdapter = new DebuggingRecyclerAdapter(DebuggingActivity.this);
        debuggingRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(DebuggingActivity.this));
        rv.setAdapter(debuggingRecyclerAdapter);
    }
}
