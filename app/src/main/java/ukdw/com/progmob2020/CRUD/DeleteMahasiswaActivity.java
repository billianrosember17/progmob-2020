package ukdw.com.progmob2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Model5.DefaultResult;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class DeleteMahasiswaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_delete);

        EditText edcari = (EditText)findViewById(R.id.editTextNim);
        Button btnDeleteNim = (Button)findViewById(R.id.btnDeleteNim);

        btnDeleteNim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(
                        edcari.getText().toString(),
                        "72180240"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(DeleteMahasiswaActivity.this, "Data Berhasil DiHapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DeleteMahasiswaActivity.this, MainMahasiswaActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(DeleteMahasiswaActivity.this, "Data Gagal DiHapus", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}