package ukdw.com.progmob2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Adapter5.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.progmob2020.Model.Mahasiswa;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class LihatMahasiswaActivity extends AppCompatActivity {
    RecyclerView rvMhs;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_lihat);

        rvMhs = (RecyclerView)findViewById(R.id.rvLihat);
        pd = new ProgressDialog(this);
        pd.setTitle("Loading");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180240");

        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatMahasiswaActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LihatMahasiswaActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}