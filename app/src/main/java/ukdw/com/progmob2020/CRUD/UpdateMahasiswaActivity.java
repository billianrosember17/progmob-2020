package ukdw.com.progmob2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Model5.DefaultResult;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class UpdateMahasiswaActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        EditText edCariNim = (EditText)findViewById(R.id.editTextCariNim);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        EditText edNimBaru = (EditText)findViewById(R.id.editTextNimBaru);
        EditText edAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaru);
        EditText edEmailBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        Button btnUp = (Button)findViewById(R.id.btnUpdateBaru);
        pd = new ProgressDialog(UpdateMahasiswaActivity.this);

        Intent data = getIntent();
        edCariNim.setText(data.getStringExtra("nim"));
        edNamaBaru.setText(data.getStringExtra("nama"));
        edNimBaru.setText(data.getStringExtra("nim"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edNamaBaru.getText().toString(),
                        edNimBaru.getText().toString(),
                        edCariNim.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        "Kosongkan",
                        "72180240"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(UpdateMahasiswaActivity.this, "Data Berhasil Diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(UpdateMahasiswaActivity.this, "Data Gagal Diupdate", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateMahasiswaActivity.this, MainMahasiswaActivity.class);
                startActivity(intent);
            }
        });
    }
}