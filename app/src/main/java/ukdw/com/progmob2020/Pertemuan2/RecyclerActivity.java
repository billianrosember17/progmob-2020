package ukdw.com.progmob2020.Pertemuan2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ukdw.com.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob2020.Model.Mahasiswa;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        //data dummy
        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Billianro Sember","           72180240", "08123456789");
        Mahasiswa m2 = new Mahasiswa("Sember Billianro","           72180241", "08123456789");
        Mahasiswa m3 = new Mahasiswa("Williandro Sember","          72180242", "08123456789");
        Mahasiswa m4 = new Mahasiswa("Sember Williandro","          72180243", "08123456789");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}